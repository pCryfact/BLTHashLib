﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CheckGetFileHash()
        {
            var hash = BLTHashLib.Hasher.GetFileHash("WSOCK32.dll");

            if (hash != "ab226640cb537f7b9355c7712cbc6ca12000776948661ae6224486f61c51a1e9")
                throw new InvalidProgramException();
        }

        [TestMethod]
        public void CheckGetDirectoryHash()
        {
            var hash = BLTHashLib.Hasher.GetDirectoryHash("Assault States");

            if (hash != "030016c3b7037d26aa2cda0d45bd491424f6e9ae700daaab07e00eb742924a1c")
                throw new InvalidProgramException();
        }

        [TestMethod]
        public void SpeedTestGetFileHash()
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var hash = BLTHashLib.Hasher.GetFileHash("WSOCK32.dll");

            stopWatch.Stop();

            Trace.WriteLine($"{stopWatch.ElapsedMilliseconds}ms");

        }

        [TestMethod]
        public void SpeedTestGetDirectoryHash()
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var hash = BLTHashLib.Hasher.GetDirectoryHash("TestStructure");

            stopWatch.Stop();

            Trace.WriteLine($"{stopWatch.ElapsedMilliseconds}ms");
        }
    }
}
